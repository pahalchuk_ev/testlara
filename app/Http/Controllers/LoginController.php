<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        return view('login.index');
    }

    public function login(Request $request)
    {
        if(\Auth::attempt(['login' => $request->login, 'password' => $request->password])){
            return redirect()->route('home');
        }else
            return redirect()->back();
    }

    public function reg()
    {
        $data = User::create([
            'login' => '111',
            'password' => bcrypt('111'),
        ]);

        dd($data);
    }
}
